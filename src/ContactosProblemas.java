import static org.junit.jupiter.api.Assertions.*;

import java.util.Scanner;

import org.junit.jupiter.api.Test;

class ContactosProblemas {

	@Test
	void test() {
		int[][][] mat = {
				{
					{1,5,6,5},
					{1,4,2,3},
					{5,4,5,8},
					{9,7,6,3}
				},
				{
					{5,8,6,8},
					{2,0,1,4},
					{0,5,8,6},
					{3,4,1,8}
				}
		};
		assertEquals("6 13 12 13\n3 4 3 7\n5 9 13 14\n12 11 7 11", ejercicio(mat));
		
	}
	
	@Test
	void test2() {
		int[][][] mat = {
				{
					{2,3,4,6},
					{7,8,8,1},
					{2,3,1,7},
					{8,1,3,0}
				},
				{
					{2,2,7,9},
					{0,0,7,8},
					{9,8,1,7},
					{5,4,3,7}
				}
		};
		assertEquals("4 5 11 15\n7 8 15 9\n11 11 2 14\n13 5 6 7", ejercicio(mat));
		
	}
	
	private String ejercicio(int[][][] mat) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
        int mat1[][] = mat[0];
        int mat2[][] = mat[1];
        int matF[][] = new int[4][4];
        
        for (int f=0; f<mat2.length; f++) {
        	for (int c=0; c<mat2[0].length; c++) {
                matF[f][c] = mat1[f][c] + mat2[f][c];
        	}
        }
        
        String r = "";
        for (int f=0; f<mat2.length; f++) {
        	for (int c=0; c < mat2[0].length; c++) {
                 if(c == mat2[0].length-1) {
                	 if(f == mat2.length-1) {
                		 r += matF[f][c];
                	 }else {
                    	 r += matF[f][c]+"\n";
                	 }
                 }else {
                	 r += matF[f][c]+" ";
                 }
        	}
        }
        
        return r;
	}

}
